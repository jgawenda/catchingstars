﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class PixelDropper
    {
        private DropletCollection _dropletCollection;
        private DropletsFrequency _dropletsFrequency;
        private int _ticksCounter;
        private int _ticksBetweenMinusDroplets;
        private int _ticksMinusCounter;

        public PixelDropper(DropletCollection dropletCollection, DropletsFrequency dropletsFrequency)
        {
            _dropletCollection = dropletCollection;
            _dropletsFrequency = dropletsFrequency;
            _ticksCounter = _dropletsFrequency.HowManyTicksToFireDroplets;
            _ticksBetweenMinusDroplets = 5;
            _ticksMinusCounter = 0;
            //_howManyTicksToFireDroplets = 3;
            //_dropletsAtOnce = 1;
        }
        
        public void Tick()
        {
            _dropletCollection.DropAllByOne();
            _dropletCollection.DisposeMissedDroplets();

            if (_ticksCounter >= _dropletsFrequency.HowManyTicksToFireDroplets)
            {
                _ticksCounter = 0;
                _ticksMinusCounter++;

                if (_ticksMinusCounter >= _ticksBetweenMinusDroplets)
                {
                    _ticksMinusCounter = 0;
                    _dropletCollection.AddNewDroplet(DropletCollection.DropletType.MinusPoints, _dropletsFrequency.DropletsAtOnce);
                }
                else
                {
                    _dropletCollection.AddNewDroplet(DropletCollection.DropletType.Normal, _dropletsFrequency.DropletsAtOnce);
                }
            }

            _ticksCounter++;
        }
    }
}
