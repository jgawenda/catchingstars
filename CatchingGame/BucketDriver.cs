﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class BucketDriver
    {
        private Bucket _bucket;
        private bool _exit;

        public BucketDriver(Bucket bucket)
        {
            _bucket = bucket;
            _exit = false;
        }

        public void RecordingKeysToMove()
        {

            do
            {
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey();

                    switch (key.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            _bucket.MoveLeft();
                            break;
                        case ConsoleKey.RightArrow:
                            _bucket.MoveRight();
                            break;
                        case ConsoleKey.Q:
                            _exit = true;
                            break;
                    }
                }

            } while (!_exit);
            
        }

        public void CenterTheBucket()
        {
            _bucket.PrepareCoords();
        }

        public void Stop()
        {
            _exit = true;
        }

    }
}
