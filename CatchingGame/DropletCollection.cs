﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class DropletCollection
    {
        public enum DropletType
        {
            Normal,
            MinusPoints
        }

        public List<Droplet> Droplets { get; }
        public int MissedDropletsCounter { get; private set; }
        private Random _random;

        public DropletCollection()
        {
            Droplets = new List<Droplet>();
            _random = new Random();
            MissedDropletsCounter = 0;
        }

        private int GetNextLegalRandom()
        {
            return _random.Next(0, Console.BufferWidth - 15);
        }

        private bool IsThereDropletAlready(int posX, int posY)
        {
            foreach (Droplet droplet in Droplets)
            {
                if (droplet.PosX == posX && droplet.PosY == posY)
                    return true;
            }

            return false;
        }

        private Droplet GetNewDropletWithRandomPosition(DropletType dropletType)
        {
            int newPosX = 0;

            do
            {
                newPosX = GetNextLegalRandom();
            } while (IsThereDropletAlready(newPosX, 0));

            switch (dropletType)
            {
                case DropletType.Normal:
                    return new NormalDroplet(newPosX, 0);
                case DropletType.MinusPoints:
                    return new MinusDroplet(newPosX, 0);
                default:
                    return new NormalDroplet(newPosX, 0);
            }
            
        }

        /*
        public void AddNewNormalDroplet(int amount)
        {
            int newPosX = 0;

            lock (Droplets)
            {
                for (int i = 0; i < amount; i++)
                {
                    do
                    {
                        newPosX = GetNextLegalRandom();
                    } while (IsThereDropletAlready(newPosX, 0));

                    Droplets.Add(new NormalDroplet(newPosX, 0));
                }
            }
            
        }
        */

        public void AddNewDroplet(DropletType dropletType, int amount)
        {
            lock (Droplets)
            {
                for (int i=0; i<amount; i++)
                {
                    Droplets.Add(GetNewDropletWithRandomPosition(dropletType));
                }
            }
        }

        public void DropAllByOne()
        {
            lock (Droplets)
            {
                foreach (Droplet droplet in Droplets)
                {
                    droplet.MoveOneDown();
                }
            }
            
        }

        public void DisposeMissedDroplets()
        {
            lock (Droplets)
            {
                for (int i = 0; i < Droplets.Count; i++)
                {
                    if (Droplets.ElementAt(i).PosY >= Console.WindowHeight)
                    {
                        if (Droplets.ElementAt(i) is NormalDroplet)
                            MissedDropletsCounter++;
                        
                        Droplets.RemoveAt(i);

                    }
                }
            }
            
        }

    }
}
