﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class Bucket
    {
        public int[] PosX { get; private set; }
        public int PosY { get; private set; }
        public string Characters { get; private set; }

        public Bucket()
        {
            PosX = new int[] { 0, 1, 2 };
            PosY = 0;
            Characters = @"\_/";
        }

        public void PrepareCoords()
        {
            // setting the first coord (get center and one to the left)
            int centerOfWindow = Console.BufferWidth / 2 - 1;

            //PosX = new int[] { centerOfWindow - 1, centerOfWindow, centerOfWindow + 1 };

            for (int i=0; i<PosX.Length; i++)
            {
                PosX[i] = centerOfWindow;
                centerOfWindow++;
            }

            PosY = Console.WindowHeight - 1;

        }

        public void MoveLeft()
        {
            if (PosX[0] > 0)
            {
                for (int i = 0; i < PosX.Length; i++)
                    PosX[i]--;
            }
        }

        public void MoveRight()
        {
            if (PosX[2] < Console.BufferWidth - 16)
            {
                for (int i = 0; i < PosX.Length; i++)
                    PosX[i]++;
            }
        }

    }
}
