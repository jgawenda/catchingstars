﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class DropletsFrequency
    {
        public enum FrequencyOfDroplets
        {
            Small,
            Medium,
            Big,
            Impossible
        }

        public FrequencyOfDroplets DropletsAmountEnum { get; private set; } = FrequencyOfDroplets.Medium;
        
        public int HowManyTicksToFireDroplets { get; private set; }
        public int DropletsAtOnce { get; private set; }

        public DropletsFrequency()
        {
            SetFrequency(FrequencyOfDroplets.Medium);
        }

        private void SetNewParameters(int ticksToFireDroplet, int dropletsAtOnce)
        {
            HowManyTicksToFireDroplets = ticksToFireDroplet;
            DropletsAtOnce = dropletsAtOnce;
        }

        private void SetFrequency(FrequencyOfDroplets frequencyOfDroplets)
        {
            DropletsAmountEnum = frequencyOfDroplets;

            switch (frequencyOfDroplets)
            {
                case FrequencyOfDroplets.Small:
                    SetNewParameters(12, 1);
                    break;
                case FrequencyOfDroplets.Medium:
                    SetNewParameters(8, 1);
                    break;
                case FrequencyOfDroplets.Big:
                    SetNewParameters(5, 1);
                    break;
                case FrequencyOfDroplets.Impossible:
                    SetNewParameters(3, 1);
                    break;
            }
        }

        public void ChangeFrequency()
        {
            int userChoice = 0;

            Console.WriteLine("\nEnter droplets frequency:");

            do
            {
                userChoice = Helper.GetIntOption("1 Small, 2 Medium, 3 Big, 4 Impossible");

                switch (userChoice)
                {
                    case 1:
                        SetFrequency(FrequencyOfDroplets.Small);
                        break;
                    case 2:
                        SetFrequency(FrequencyOfDroplets.Medium);
                        break;
                    case 3:
                        SetFrequency(FrequencyOfDroplets.Big);
                        break;
                    case 4:
                        SetFrequency(FrequencyOfDroplets.Impossible);
                        break;
                }

            } while (userChoice < 1 || userChoice > 4);

        }
    }
}
