﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    public abstract class Droplet
    {
        public int PosX { get; private set; }
        public int PosY { get; private set; }
        public char Character { get; protected set; }

        public Droplet(int x, int y)
        {
            PosX = x;
            PosY = y;
        }

        public void MoveOneDown()
        {
            PosY++;
        }
        
    }
}
