﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class ScreenViewer
    {
        private DropletCollection _dropletCollection;
        private Bucket _bucket;
        private ScoreCounter _scoreCounter;

        public ScreenViewer(DropletCollection dropletCollection, Bucket bucket, ScoreCounter scoreCounter)
        {
            _dropletCollection = dropletCollection;
            _bucket = bucket;
            _scoreCounter = scoreCounter;
        }

        private void DisplayDroplets()
        {
            // we have to dispose missed droplets before displaying them
            // it will prevent the exception "Collection was modified; enumeration operation may not execute"
            // when operating on threads
            //_dropletCollection.DisposeMissedDroplets();

            lock (_dropletCollection.Droplets)
            {
                foreach (Droplet droplet in _dropletCollection.Droplets)
                {
                    Console.SetCursorPosition(droplet.PosX, droplet.PosY);
                    Console.Write(droplet.Character);
                }
            }
            
        }

        private void DisplayBucket()
        {
            Console.SetCursorPosition(_bucket.PosX[0], _bucket.PosY);
            Console.Write(_bucket.Characters);
        }

        private void DisplayScore()
        {
            Console.SetCursorPosition(Console.BufferWidth - 14, 0);
            Console.Write("Score: {0}", _scoreCounter.TotalScore);
        }

        private void DisplayMissedDroplets()
        {
            Console.SetCursorPosition(Console.BufferWidth - 14, 2);
            Console.Write("Missed: {0}", _dropletCollection.MissedDropletsCounter);
        }
        
        private void ClearLastLine()
        {
            Console.SetCursorPosition(0, Console.WindowHeight - 1);
            Console.Write(new string(' ', Console.WindowWidth - 1));
            Console.SetCursorPosition(0, 0);
        }

        public void ViewBucket()
        {
            ClearLastLine();
            DisplayBucket();
        }

        public void ViewAll()
        {
            Console.Clear();
            DisplayDroplets();
            DisplayBucket();
            DisplayScore();
            DisplayMissedDroplets();
            Console.SetCursorPosition(0, 0);
        }
    }
}
