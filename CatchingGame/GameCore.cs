﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class GameCore
    {
        private PixelDropper _pixelDropper;
        private ScreenViewer _screenViewer;
        private BucketDriver _bucketDriver;
        // keep this here as long as we want to have the _scoreCounter.Check() in GameCore class
        private ScoreCounter _scoreCounter;
        private ThreadsManager _threadsManager;

        public GameOptions GameOptions { get; }

        public GameCore()
        {
            GameOptions = new GameOptions();

            DropletCollection dropletCollection = new DropletCollection();
            _pixelDropper = new PixelDropper(dropletCollection, GameOptions.DropletsFrequency);
            
            Bucket bucket = new Bucket();
            _bucketDriver = new BucketDriver(bucket);

            _scoreCounter = new ScoreCounter(dropletCollection, bucket);
            
            _screenViewer = new ScreenViewer(dropletCollection, bucket, _scoreCounter);

            GameOverManager gameOverManager = new GameOverManager(dropletCollection, GameOptions.GameOverConditions);
            
            _threadsManager = new ThreadsManager(_pixelDropper, _screenViewer, _bucketDriver, _scoreCounter, GameOptions, gameOverManager);

        }

        private void DisplayRulesAndInstructions()
        {
            Console.Clear();
            Console.WriteLine("Rules:");
            Console.WriteLine("Your goal is to catch the stars * .");
            Console.WriteLine("For every * you catch, you receive points.");
            Console.WriteLine("Don't let the stars fall to the ground.\n");
            Console.WriteLine("* = 1 point");
            Console.WriteLine("\n----\nGame settings:");
            GameOptions.DisplaySettings();
            Console.WriteLine("\n----\nControls:");
            Console.WriteLine("Move left: <- (left arrow)");
            Console.WriteLine("Move right: -> (right arrow)");
            Console.WriteLine("Stop the game: Q");
            Console.Write("\n\nPress enter to begin");
            Console.ReadLine();
        }
        
        public void StartGame()
        {
            _bucketDriver.CenterTheBucket();
            DisplayRulesAndInstructions();
            Console.CursorVisible = false;

            /*
                _pixelDropper.Tick();
                Console.SetCursorPosition(0, 0);
                _bucketDriver.RecordingKeysToMove();
                _scoreCounter.Check();
                _screenViewer.ViewAll();
                */

            //_screenViewer.ViewBucket();
            // dropper.Tick, scoreCounter.Check, screenViewer.ViewStarsAndScore - can be in one thread
            // bucketDriver.RecordingKeysToMove, screenViewer.ViewBucket - another thread, refresh rate high
            // OR
            // dropper.Tick, scoreCounter.Check - one thread, low refresh rate
            // bucketDriver.RecordingKeysToMove - second thread, loop while keypressed != q
            // screenViewer.ViewAll - third thread, high refresh rate

            _threadsManager.Start();

        }

    }
}
