﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    public class NormalDroplet : Droplet
    {
        public int PointValue { get; private set; }

        public NormalDroplet(int x, int y) : base (x, y)
        {
            Character = '*';
            PointValue = 1;
        }
        
    }
}
