﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class GameOverManager
    {
        public bool GameIsOver { get; private set; } = false;
        private DropletCollection _dropletCollection;
        private GameOverConditions _gameOverConditions;

        public GameOverManager(DropletCollection dropletCollection, GameOverConditions gameOverConditions)
        {
            _dropletCollection = dropletCollection;
            _gameOverConditions = gameOverConditions;
        }

        public void Check()
        {
            if (_dropletCollection.MissedDropletsCounter >= _gameOverConditions.DropletsToMiss)
                GameIsOver = true;

        }

        public void StopTheGame()
        {
            GameIsOver = true;
        }
    }
}
