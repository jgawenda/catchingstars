﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class GameOverConditions
    {
        public int DropletsToMiss { get; private set; } = 1;

        private void DisplayInfo()
        {
            Console.WriteLine("Game over conditions:");
            Console.WriteLine("{0} stars missed till game over", DropletsToMiss);
            Console.WriteLine("-----\n");
        }

        public void Change()
        {
            //DisplayInfo();

            DropletsToMiss = Helper.GetIntOption(1, 99, "How many missed stars till game over?");
            
        }
    }
}
