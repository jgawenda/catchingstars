﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class MinusDroplet : Droplet
    {
        public MinusDroplet(int x, int y) : base (x, y)
        {
            Character = '#';
        }
    }
}
