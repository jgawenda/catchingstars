﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CatchingGame
{
    class ThreadsManager
    {
        private PixelDropper _pixelDropper;
        private ScreenViewer _screenViewer;
        private BucketDriver _bucketDriver;
        private ScoreCounter _scoreCounter;
        private GameOptions _gameOptions;
        private GameOverManager _gameOverManager;

        private Thread _threadTickAndCheck;
        private Thread _threadRecordBucketMovement;
        private Thread _threadDisplayAll;

        public ThreadsManager(PixelDropper pixelDropper, ScreenViewer screenViewer, BucketDriver bucketDriver, ScoreCounter scoreCounter, GameOptions gameOptions, GameOverManager gameOverManager)
        {
            // initialize objects to work on
            _pixelDropper = pixelDropper;
            _screenViewer = screenViewer;
            _bucketDriver = bucketDriver;
            _scoreCounter = scoreCounter;
            _gameOptions = gameOptions;
            _gameOverManager = gameOverManager;

            // initialize threads
            _threadTickAndCheck = new Thread(new ThreadStart(TickAndCheck));
            _threadRecordBucketMovement = new Thread(new ThreadStart(RecordBucketMovement));
            _threadDisplayAll = new Thread(new ThreadStart(DisplayAll));

        }

        private void TickAndCheck()
        {
            int sleepTime = 1000 / _gameOptions.Speed;

            do
            {
                Thread.Sleep(sleepTime);
                _pixelDropper.Tick();
                _scoreCounter.Check();
                _gameOverManager.Check();
            } while (!_gameOverManager.GameIsOver);

            //stop bucket driver after the game is over
            _bucketDriver.Stop();
        }

        private void RecordBucketMovement()
        {
            _bucketDriver.RecordingKeysToMove();

            //send stop signal to the manager in order to finish the game
            _gameOverManager.StopTheGame();
        }

        private void DisplayAll()
        {
            do
            {
                Thread.Sleep(50);
                _screenViewer.ViewAll();

            } while (!_gameOverManager.GameIsOver);
        }

        public void Start()
        {
            _threadRecordBucketMovement.Start();
            _threadTickAndCheck.Start();
            _threadDisplayAll.Start();
            _threadDisplayAll.Join();

            Console.Clear();
            Console.CursorVisible = true;
            Console.WriteLine("Game Over. Your score: {0}", _scoreCounter.TotalScore);
            Console.WriteLine("\nThanks for playing! :)");
            Console.ReadLine();

        }

    }
}
