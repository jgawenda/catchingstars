﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class ScoreCounter
    {
        private DropletCollection _dropletCollection;
        private Bucket _bucket;
        public int TotalScore { get; private set; } = 0;

        private IEnumerable<Droplet> _dropletsInBucket;
        private int _howManyDropletsInBucket = 0;

        public ScoreCounter(DropletCollection dropletCollection, Bucket bucket)
        {
            _dropletCollection = dropletCollection;
            _bucket = bucket;
        }

        private void ProcessTheScoring(Droplet droplet)
        {
            if (droplet is NormalDroplet)
            {
                NormalDroplet normalDroplet = (NormalDroplet)droplet;
                TotalScore += normalDroplet.PointValue;
            }
            else if (droplet is MinusDroplet)
            {
                MinusDroplet minusDroplet = (MinusDroplet)droplet;
                TotalScore--;
            }
            // here we can add more droplet types processing
        }

        private void CheckIfStarsAreInBucket()
        {

            lock (_dropletCollection.Droplets)
            {
                /*
                howManyDropletsInBucket = _dropletCollection.Droplets
                .Where(p => p.PosY == Console.WindowHeight - 1)
                .Count(p => p.PosX == _bucket.PosX[0] || p.PosX == _bucket.PosX[1] || p.PosX == _bucket.PosX[2]);
                */

                /*
                howManyDropletsInBucket = _dropletCollection.Droplets
                    .RemoveAll(p => p.PosY == Console.WindowHeight - 1 && (p.PosX == _bucket.PosX[0] || p.PosX == _bucket.PosX[1] || p.PosX == _bucket.PosX[2]));
                */

                _dropletsInBucket = _dropletCollection.Droplets
                    .Where(p => p.PosY == Console.WindowHeight - 1 && (p.PosX == _bucket.PosX[0] || p.PosX == _bucket.PosX[1] || p.PosX == _bucket.PosX[2]));

                _howManyDropletsInBucket = _dropletsInBucket.Count();
                
                if (_howManyDropletsInBucket > 0)
                {
                    //TotalScore += howManyDropletsInBucket;
                    

                    for (int i=0; i<_howManyDropletsInBucket; i++)
                    {
                        ProcessTheScoring(_dropletsInBucket.ElementAt(i));
                        _dropletCollection.Droplets.Remove(_dropletsInBucket.ElementAt(i));
                    }
                }
                
            }

        }

        public void Check()
        {
            CheckIfStarsAreInBucket();
        }
    }
}
