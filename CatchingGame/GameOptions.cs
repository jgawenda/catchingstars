﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class GameOptions
    {
        public int WindowHeight { get; private set; } = Console.WindowHeight;
        public int WindowWidth { get; private set; } = Console.WindowWidth;
        public int Speed { get; private set; } = 4;
        public DropletsFrequency DropletsFrequency { get; } = new DropletsFrequency();
        public GameOverConditions GameOverConditions { get; } = new GameOverConditions();

        private int _minHeight = 10;
        private int _minWidth = 35;

        private int _minSpeed = 1;
        private int _maxSpeed = 15;
        
        private bool ChangeHeight(int newHeight)
        {
            if (newHeight >= _minHeight && newHeight <= Console.LargestWindowHeight)
            {
                WindowHeight = newHeight;
                return true;
            }
            else
            {
                Console.WriteLine("This height is not proper (min {0}, max {1}).", _minHeight ,Console.LargestWindowHeight);
                Console.ReadLine();
                return false;
            }
        }

        private bool ChangeWidth(int newWidth)
        {
            if (newWidth >= _minWidth && newWidth <= Console.LargestWindowWidth)
            {
                WindowWidth = newWidth;
                return true;
            }
            else
            {
                Console.WriteLine("This width is not proper (min {0}, max {1}).", _minWidth ,Console.LargestWindowWidth);
                Console.ReadLine();
                return false;
            }
        }

        private bool ChangeSpeed(int newSpeed)
        {
            if (newSpeed > 0 && newSpeed <= _maxSpeed)
            {
                Speed = newSpeed;
                return true;
            }
            else
            {
                Console.WriteLine("Speed is not proper (min {0}, max {1}).", _minSpeed, _maxSpeed);
                Console.ReadLine();
                return false;
            }
        }
        
        public void DisplaySettings()
        {
            Console.WriteLine("Height: {0}", WindowHeight);
            Console.WriteLine("Width: {0}", WindowWidth);
            Console.WriteLine("Speed: {0}", Speed);
            Console.WriteLine("Stars frequency: {0}", DropletsFrequency.DropletsAmountEnum.ToString());
            Console.WriteLine("How many stars till game over: {0}", GameOverConditions.DropletsToMiss);
        }

        private void ApplyChanges()
        {
            Console.WindowHeight = WindowHeight;
            Console.WindowWidth = WindowWidth;

            AdjustBufferSize();
        }

        public void AdjustBufferSize()
        {
            Console.BufferHeight = Console.WindowHeight;
            Console.BufferWidth = Console.WindowWidth;
        }
        
        public void ChangeSettings()
        {
            Console.Clear();
            Console.WriteLine("[Settings]\n");
            Console.WriteLine("----------");
            DisplaySettings();
            Console.WriteLine("----------\n");

            int newHeight = 0;
            int newWidth = 0;
            int newSpeed = 0;

            do
            {
                newHeight = Helper.GetIntOption(String.Format("Enter new HEIGHT ({0}-{1})", _minHeight, Console.LargestWindowHeight));
            } while (!ChangeHeight(newHeight));

            do
            {
                newWidth = Helper.GetIntOption(String.Format("Enter new WIDTH ({0}-{1})", _minWidth, Console.LargestWindowWidth));
            } while (!ChangeWidth(newWidth));

            do
            {
                newSpeed = Helper.GetIntOption(String.Format("Enter new SPEED ({0}-{1})", _minSpeed, _maxSpeed));
            } while (!ChangeSpeed(newSpeed));

            DropletsFrequency.ChangeFrequency();

            GameOverConditions.Change();

            ApplyChanges();
        }

        public void ChangeWindowSize(int windowHeight, int windowWidth)
        {
            ChangeHeight(windowHeight);
            ChangeWidth(windowWidth);
            ApplyChanges();
        }

    }
}
