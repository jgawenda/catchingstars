﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    static class Helper
    {
        public static int GetIntOption()
        {
            int output = 0;
            string userInput = "";

            do
            {
                userInput = Console.ReadLine();
            } while (!Int32.TryParse(userInput, out output));

            return output;
        }

        public static int GetIntOption(string message)
        {
            int output = 0;
            string userInput = "";

            do
            {
                Console.Write("{0}: ", message);
                userInput = Console.ReadLine();
            } while (!Int32.TryParse(userInput, out output));

            return output;
        }

        public static int GetIntOption(int min, int max, string message)
        {
            int output = 0;

            //if min is bigger than max, switch them
            if (min > max)
            {
                int tempInt = min;
                min = max;
                max = tempInt;
            }

            do
            {
                output = GetIntOption(String.Format("{0} ({1}-{2})", message, min, max));

            } while (output < min || output > max);

            return output;

        }

    }
}
