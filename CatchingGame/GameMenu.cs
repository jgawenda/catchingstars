﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingGame
{
    class GameMenu
    {
        private GameCore _gameCore = new GameCore();

        private void ShowOptions()
        {
            Console.WriteLine("[Catching game]\n");
            Console.WriteLine("1. Start the game");
            Console.WriteLine("2. Show highscores");
            Console.WriteLine("3. Change game options");
            Console.WriteLine("0. Exit game");
        }

        public void Show()
        {
            //_gameCore.GameOptions.AdjustBufferSize();
            _gameCore.GameOptions.ChangeWindowSize(30, 45);
            int userChoice = 0;

            do
            {
                Console.Clear();
                ShowOptions();
                userChoice = Helper.GetIntOption("Your choice");

                switch (userChoice)
                {
                    case 1:
                        _gameCore.StartGame();
                        userChoice = 0;
                        break;
                    case 2:
                        break;
                    case 3:
                        _gameCore.GameOptions.ChangeSettings();
                        break;
                    case 0:
                        break;
                    default:
                        break;
                }

            } while (userChoice != 0);

        }
    }
}
